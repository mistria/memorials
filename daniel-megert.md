# In Memory of Daniel Megert



Please add your contribution below via a merge request. 

## Denis Roy

Daniel was passionate about all things Eclipse, and worked tirelessly to make things better. Easily approachable and personable, he would engage constructively to challenge status quo and was very understanding of the constraints that were slowing progress. 

## Jonah Graham

Thank you to Dani for your tireless work on Eclipse. Your absence will be sorely missed. You leave behind an excellent legacy of a mature and stable Eclipse project that you dedicated so many years of your life contributing to and improving.
